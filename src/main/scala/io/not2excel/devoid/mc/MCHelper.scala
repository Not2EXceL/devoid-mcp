package io.not2excel.devoid.mc

object MCHelper {

    val isObfuscated: Boolean = {
        try {
            Class.forName("net.minecraft.client.Minecraft")
            true
        } catch {
            case e: ClassNotFoundException => false
        }
    }
}
